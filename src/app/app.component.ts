import { Component } from '@angular/core';
import { AuthService } from './common/services/auth.service';
import { UserService } from './common/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private _authService: AuthService,
    private _userService: UserService
  ) { }

  public isAuthorized(): boolean {
    return this._userService.isAuthorized;
  }

  public logout(): void {
    this._authService.logout().subscribe(response => {
      this._userService.logout()
    });
  }
}