import { RegistrationData } from './../../common/Models/registration-data';
import { AuthService } from './../../common/services/auth.service';
import { Component } from '@angular/core';

@Component({
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  public data: RegistrationData = new RegistrationData();
  public isVisiblePassword: boolean = false;

  constructor(
    private _authService: AuthService
  ) { }

  public onToogleVisibility() {
    this.isVisiblePassword = !this.isVisiblePassword;
  }

  public onSubmit() {
    this._authService.registration(this.data).subscribe(
      response => {
        if (response.success) {
          console.log("Удачно");
        } else {
          console.log(response.errorMessage);
        }
      }
    );
  }
}
