import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent {

  constructor(private http: HttpClient) {
  }

  onSubmit() {
    this.http.get(`https://stocksharing.azurewebsites.net/api/admin/getUsers`).subscribe();
  }

  title = 'HomeComponent';
}
