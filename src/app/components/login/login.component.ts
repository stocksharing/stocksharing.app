import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from './../../common/services/auth.service';
import { UserService } from './../../common/services/user.service';
import { Component } from '@angular/core';
import { LoginData } from './../../common/Models/login-data';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public data: LoginData = new LoginData();
  public isVisiblePassword: boolean = false;

  constructor(
    private _authService: AuthService,
    private _userService: UserService,
    private _matSnackBar: MatSnackBar
  ) { }

  public onToogleVisibility() {
    this.isVisiblePassword = !this.isVisiblePassword;
  }

  public onSubmit() {
    this._authService.login(this.data).subscribe(
      response => {
        if (response.success) {
          this._userService.login(response.user);
        } else {
          this._matSnackBar.open(response.errorMessage, "X", {
            duration: 2000,
            horizontalPosition: "right"
          });
        }
      },
      error => {
        this._matSnackBar.open(error, "X", {
          duration: 2000,
          horizontalPosition: "right"
        });
      }
    );
  }
}
