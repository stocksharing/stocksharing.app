import { Observable } from 'rxjs';
import { LoginData } from './../Models/login-data';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegistrationData } from '../Models/registration-data';
import { LoginResponse } from '../Models/login-response';
import { RegistrationResponse } from '../Models/registration-response';

@Injectable({ providedIn: 'root' })
export class AuthService {

    constructor(private http: HttpClient) { }

    public login(data: LoginData): Observable<LoginResponse> {
        return this.http.post<LoginResponse>(`${environment.baseUrl}/api/account/login`, data);
    }

    public logout(): Observable<object> {
        return this.http.get(`${environment.baseUrl}/api/account/logout`);
    }

    public registration(data: RegistrationData): Observable<RegistrationResponse> {
        return this.http.post<RegistrationResponse>(`${environment.baseUrl}/api/account/registration`, data);
    }
}