import { User } from './../Models/user';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UserService {
    get user(): User | null {
        let data = localStorage.getItem('user');
        return data === null ? null : JSON.parse(data);
    }

    get isAuthorized(): boolean {
        return this.user !== null;
    }

    public login(data: User): void {
        localStorage.setItem('user', JSON.stringify(data));
    }

    public logout(): void {
        localStorage.removeItem('user');
    }
}