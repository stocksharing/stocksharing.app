export class RegistrationResponse {
    public success: boolean;
    public errorMessage: string;
}
