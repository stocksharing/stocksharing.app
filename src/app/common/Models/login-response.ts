import { User } from './user';

export class LoginResponse {
    public success: boolean;
    public errorMessage: string;
    public user: User;
}
